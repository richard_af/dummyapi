import React from 'react';
import {
  SafeAreaView, 
  ScrollView, 
  StatusBar
} from 'react-native';

// Code bisa dipakai untuk (GET, POST, PUT, DELETE) dikarenakan LocalAPI.
import JsRestApi from './LocalApi/jsFetch'; // <--- Code untuk menjalankan Fake API (LocalAPI)
import LocalApi from './LocalApi/localApi'; // <--- Code untuk 3rd Party Axios menggunakan Fake API (LocalAPI)

// Code bawaan dari YT untuk referensi.
import AxiosRestApi from './LocalApi/callAxios'; // <--- Code murni bawaan untuk menggunakan 3rd Party Axios
import VanillaRestApi from './LocalApi/vanillaJS'; // <--- Code murni bawaan untuk Fetch VanillaJS

// Code tidak bisa dipakai untuk (POST, PUT, DELETE) dikarenakan Online API khusus 'GET'.
import GetDataAPI from './OnlineAPI/GetAPI'; // <--- GET Data + Review UI, code nya sudah benar (CRUD) dalam method VanillaJS.
import GetDataAxios from './OnlineAPI/GetDataAxios'; // <--- GET Data + Review UI, (CRUD) menggunakan 3rd Party Axios.

// npx react-native run-android
// Wajib Online, kalau ngga ya gabisa dapet data online
// Coba cek Metro (log) biar bisa check apakah data dari Json nya udah di Get atau belum, baru cek di UI.

// Kalau ga dapet data dari Local API, kemungkinan salah IP atau data yg diterima.
// Kalau ga dapet data dari online API, kemungkinan Inet bermasalah atau lemot.

export default function App() {
  return (
    <SafeAreaView>
      <ScrollView>
        <StatusBar hidden />
        {/* <JsRestApi /> */}
        {/* <LocalApi /> */}
        {/* <AxiosRestApi /> */}
        {/* <VanillaRestApi /> */}
        {/* <GetDataAPI /> */}
        <GetDataAxios />
      </ScrollView>
    </SafeAreaView>
  );
}
import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
} from 'react-native';

// Install : https://github.com/typicode/json-server ( npm install -g json-server )

// Watch Json Server : json-server --watch db.json
//                     json-server --watch db.json --port 3004 ( http://localhost:3000/_ _ /_ _ )

// create db.json

export default function AxiosRestApi() {
  const [dataUser, setDataUser] = useState({
    avatar: '',
    first_name: '',
    last_name: '',
    email: '',
  });

  const [jobUser, setJobUser] = useState({
    name: '',
    job: '',
  });

  //GET
  const getData = () => {
    axios
      .get('https://reqres.in/api/users/6')
      .then(result => {
        console.log('Success');
        setDataUser(result.data.data);
      })
      .catch(err => console.log('error: ', err));
  };

  //POST
  const postData = () => {
    const dataPostApi = {
      name: 'morpheus',
      job: 'leader',
    };

    axios
      .post('https://reqres.in/api/users', dataPostApi)
      .then(result => {
        console.log('Success');
        setJobUser(result.data);
      })
      .catch(err => console.log('error: ', err));
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar hidden />

      <View style={{marginVertical: 2}}>
        <Text style={styles.text}>Hello Axios Json</Text>
        <Button title="GET DATA" onPress={getData} />
        <Text style={{marginVertical: 5}}>Res: Get Data</Text>

        {dataUser.avatar.length > 0 && (
          <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
        )}

        <Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
        <Text>{dataUser.email}</Text>
      </View>

      <View style={styles.line} />

      <View>
        <Button title="POST DATA" onPress={postData} />
        <Text style={{marginTop: 5}}>Res: Post Data</Text>
        <Text>{jobUser.name}</Text>
        <Text>{jobUser.job}</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  text: {
    marginVertical: 15,
    textAlign: 'center',
    fontSize: 15,
  },
  line: {
    height: 1,
    backgroundColor: 'black',
    marginVertical: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 100,
  },
});

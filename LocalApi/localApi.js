import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { 
  StyleSheet,
  Text, 
  View,
  SafeAreaView,
  TextInput,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';

// Install : https://github.com/typicode/json-server ( npm install -g json-server )
// Testing Using Postman Agent (GET)
// 'cd database' di terminal, biar file db.json yang ada di folder 'database' nya bisa di watch
// Watch Json Server :  json-server --watch db.json 
//                      json-server --watch db.json --port 3004
//                      json-server --host localhost db.json --port 3004
//                      json-server --host 192.168.xx.xxx db.json --port 3004

const Data = ({ fullName, email, division, onPress, onDelete }) => {

  return (
    <View style={styles.data}>
      <TouchableOpacity 
        style={{ flex: 1, flexDirection: 'row', marginRight: 20,}} 
        onPress={onPress}
      >
        <Image 
          source={{uri: 'https://cdn4.iconfinder.com/data/icons/men-avatars-icons-set-2/256/5-512.png'}} 
          style={styles.avatar}  
        />
        <View style={styles.dataDescription}>
          <Text style={styles.dataName}>{fullName}</Text>   
          {/* {fullName} */}
          <Text style={styles.dataEmail}>{email}</Text>    
          {/* {email} */}
          <Text style={styles.dataDivision}>{division}</Text>   
          {/* {division} */}
        </View>
      </TouchableOpacity>
        
      <TouchableOpacity 
        style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center', right: 10,}}
        onPress={onDelete}
      >
        <Image source={require('../img/delete.png')} style={styles.delete} />
      </TouchableOpacity>
      
    </View>
  )

}

const LocalApi = () => {

  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [division, setDivision] = useState("");
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Register');
  const [selected, setSelected] = useState({});

  useEffect(() => {
    getData();
  }, []);

  const submit = () => {

    const axios = require('axios').default

    const data = {
      fullName,
      email,
      division
    }

    if(button === 'Register'){
      console.log('data send: ', data);
      axios.post('http://192.168.xx.xxx:3004/users', data) //10.0.2.2 //192.168.xx.xxx
      .then(res => {
        console.log('res :', res);
        setFullName("");
        setEmail("");
        setDivision("");
        getData();
      })
      .then(err => console.log('error: ', err));
    } else if(button === 'Update'){
      axios.put(`http://192.168.xx.xxx:3004/users/${selected.id}`, data)
      .then(res => {
        console.log('res update: ', res);
        getData();
        setFullName("");
        setEmail("");
        setDivision("");
        setButton('Register');
      })
    }
    
  }

  const getData = () => {
    axios.get('http://192.168.xx.xxx:3004/users')
    .then(res => {
      console.log('res Get Data: ', res)
      setUsers(res.data);
    })
  }

  const selection = (item) => {
    console.log('selected :', item)
    setSelected(item);
    setFullName(item.fullName);
    setEmail(item.email);
    setDivision(item.division);
    setButton('Update');
  } 
  
  const deleteItem = (item) => {
    console.log(item);
    axios.delete(`http://192.168.xx.xxx:3004/users/${item.id}`)
    .then(res => {
      console.log("res delete: ", res);
      getData();
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1 , padding:  2, }}>
        <Text style={styles.titleText}>Local API Json Server</Text>
        <Text style={styles.text}>Daftar Anggota Divisi</Text>

        <TextInput 
          placeholder="Nama Lengkap" 
          style={styles.input} 
          value={fullName} 
          onChangeText={(value) => setFullName(value)}
        />

        <TextInput 
          placeholder="Email" 
          style={styles.input} 
          keyboardType={'email-address'}
          value={email} 
          onChangeText={(value) => setEmail(value)}
        />

        <TextInput 
          placeholder="Divisi" 
          style={styles.input} 
          value={division} 
          onChangeText={(value) => setDivision(value)}
        />

        <View style={styles.button}>
          <Button title={button} onPress={submit}/>
        </View>
        <View style={styles.line}/>

        <ScrollView>
          {users.map(user => {
            return (
              <Data 
                key={user.id} 
                fullName={user.fullName} 
                email={user.email} 
                division={user.division}
                onPress={() => selection(user)}
                onDelete={() => 
                  Alert.alert(
                    'Warning', 
                    'Are you sure want to delete this User?',
                    [
                      {
                        text: 'No', 
                        onPress: () => {}
                      },
                      {
                        text: 'Yes', 
                        onPress: () => deleteItem(user)
                      }
                    ]
                  )
                }
              />
            )
          })}
          {/* <Data />
          <Data /> */}
        </ScrollView>

      </View>
    </SafeAreaView>
  )
}

export default LocalApi

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  titleText: {
    textAlign: 'center',
    marginVertical: 8,
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 1.1,
  },
  text: {
    fontSize: 14,
    marginBottom: 10,
  },
  button: {
    marginVertical: 3,
    alignSelf: 'center',
    width: '55%',
  },
  line: {
    height: 1.2,
    backgroundColor: 'black',
    marginVertical: 10,
  },
  input: {
    borderWidth: 0.7,
    marginBottom: 10,
    borderRadius: 10,
    paddingHorizontal: 14,
  },
  data: {
    flexDirection: 'row',
    padding: 6,
    marginVertical: 5,
    // backgroundColor: '#'
    borderRadius: 7,
    borderWidth: 0.78,
  },
  dataDescription: {
    marginLeft: 14,
    flex: 1,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 100,
    backgroundColor: 'white'
  },
  dataName: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  dataEmail: {
    fontSize: 12,
    color: '#AEAEAE',
    marginTop: 2,
  },
  dataDivision: {
    fontSize: 14,
    marginTop: 8,
    letterSpacing: 0.7,
  },
  delete: {
    width: 15,
    height: 18,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
})

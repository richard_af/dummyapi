import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
} from 'react-native';

export default function VanillaRestApi() {
  const [dataUser, setDataUser] = useState({
    avatar: '',
    first_name: '',
    last_name: '',
    email: '',
  });

  const [jobUser, setJobUser] = useState({
    name: '',
    job: '',
  });

  useEffect(() => {
    // //Call API ( GET )
    // fetch('https://freegeoip.app/json/') //default option GET
    // .then(res => res.json())
    // .then(json => console.log(json))
    // //Call API ( POST )
    // const dataPostApi = {
    //   name: "morpheus",
    //   job: "leader"
    // }
    // fetch('https://reqres.in/api/users', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify(dataPostApi)
    // })
    // .then(res => res.json())
    // .then(json => console.log(json))
  }, []);

  const getData = () => {
    fetch('https://reqres.in/api/users/2')
      .then(res => res.json())
      .then(json => {
        console.log(json), setDataUser(json.data);
      });
  };

  const postData = () => {
    const dataPostApi = {
      name: 'morpheus',
      job: 'leader',
    };

    fetch('https://reqres.in/api/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(dataPostApi),
    })
      .then(res => res.json())
      .then(json => {
        console.log(json);
        setJobUser(json);
      });
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar hidden />
      <Text style={styles.text}>Hello API Json</Text>
      <View style={{marginVertical: 10}}>
        <Button title="GET DATA" onPress={getData} />
        <Text style={{marginVertical: 5}}>Res: Get Data</Text>
        {dataUser.avatar.length > 0 && (
          <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
        )}
        <Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
        <Text>{dataUser.email}</Text>
      </View>
      <View style={styles.line} />
      <View>
        <Button title="POST DATA" onPress={postData} />
        <Text style={{marginTop: 5}}>Res: Post Data</Text>
        <Text>{jobUser.name}</Text>
        <Text>{jobUser.job}</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  text: {
    marginVertical: 15,
    textAlign: 'center',
    fontSize: 15,
  },
  line: {
    height: 1,
    backgroundColor: 'black',
    marginVertical: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 100,
  },
});

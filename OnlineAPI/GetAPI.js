import React, { useState, useEffect } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  SafeAreaView,
  TextInput,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';

const Data = ({ name, email, username, onPress, onDelete }) => {

  return (
    <View style={styles.data}>
      <TouchableOpacity 
        style={{ flex: 1, flexDirection: 'row', marginRight: 20,}} 
        onPress={onPress}
      >
        <Image 
          source={{uri: 'https://cdn4.iconfinder.com/data/icons/men-avatars-icons-set-2/256/5-512.png'}} 
          style={styles.avatar}  
        />
        <View style={styles.dataDescription}>
          <Text style={styles.dataName}>{name}</Text>   
          {/* {fullName} */}
          <Text style={styles.dataEmail}>{email}</Text>    
          {/* {email} */}
          <Text style={styles.dataUsername}>{username}</Text>   
          {/* {division} */}
        </View>
      </TouchableOpacity>
        
      <TouchableOpacity 
        style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center', right: 10,}}
        onPress={onDelete}
      >
        <Image source={require('../img/delete.png')} style={styles.delete} />
      </TouchableOpacity>
      
    </View>
  )

}

export default function GetDataAPI () {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Register');
  const [selected, setSelected] = useState({});

  const getData = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(json => {
      console.log('Get Data: ', json)
      setUsers(json);
    })
  }

  useEffect(() => {
    getData();
  }, []);

  const submit = () => {

    const data = {
      name,
      email,
      username
    }

    if(button === 'Register'){
      console.log('data send: ', data);
      fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(res => res.json())
      .then(json => {
        console.log('res :', json);
        setName("");
        setEmail("");
        setUsername("");
        getData();
      })
      .then(err => console.log('error: ', err));
    } else if(button === 'Update'){
      fetch(`https://jsonplaceholder.typicode.com/users/${selected.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(res => res.json())
      .then(json => {
        console.log('res update: ', json);
        getData();
        setName("");
        setEmail("");
        setUsername("");
        setButton('Register');
      })
    }
    
  }

  const selection = (item) => {
    console.log('selected :', item)
    setSelected(item);
    setName(item.name);
    setEmail(item.email);
    setUsername(item.username);
    setButton('Update');
  } 
  
  const deleteItem = (item) => {
    console.log(item);
    fetch(`https://jsonplaceholder.typicode.com/users/${item.id}`, {
      method: 'DELETE',
    })
    .then(res => res.json())
    .then(json => {
      console.log("res delete: ", json);
      getData();
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1 , padding:  2, }}>
        <Text style={styles.titleText}>Local API Json Server</Text>
        <Text style={styles.text}>Daftar Anggota Divisi</Text>

        <TextInput 
          placeholder="Nama Lengkap" 
          style={styles.input} 
          value={name} 
          onChangeText={(value) => setName(value)}
        />

        <TextInput 
          placeholder="Email" 
          style={styles.input} 
          keyboardType={'email-address'}
          value={email} 
          onChangeText={(value) => setEmail(value)}
        />

        <TextInput 
          placeholder="Username" 
          style={styles.input} 
          value={username} 
          onChangeText={(value) => setUsername(value)}
        />

        <View style={styles.button}>
          <Button title={button} onPress={submit}/>
        </View>
        <View style={styles.line}/>

        <ScrollView>
          {users.map(user => {
            return (
              <Data 
                key={user.id} 
                name={user.name} 
                email={user.email} 
                username={user.username}
                onPress={() => selection(user)}
                onDelete={() => 
                  Alert.alert(
                    'Warning', 
                    'Are you sure want to delete this User?',
                    [
                      {
                        text: 'No', 
                        onPress: () => {}
                      },
                      {
                        text: 'Yes', 
                        onPress: () => deleteItem(user)
                      }
                    ]
                  )
                }
              />
            )
          })}
          {/* <Data /> */}
        </ScrollView>

      </View>
    </SafeAreaView>
  )
}

// <Data />

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  titleText: {
    textAlign: 'center',
    marginVertical: 8,
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 1.1,
  },
  text: {
    fontSize: 14,
    marginBottom: 10,
  },
  button: {
    marginVertical: 3,
    alignSelf: 'center',
    width: '55%',
  },
  line: {
    height: 1.2,
    backgroundColor: 'black',
    marginVertical: 10,
  },
  input: {
    borderWidth: 0.7,
    marginBottom: 10,
    borderRadius: 10,
    paddingHorizontal: 14,
  },
  data: {
    flexDirection: 'row',
    padding: 6,
    marginVertical: 5,
    // backgroundColor: '#'
    borderRadius: 7,
    borderWidth: 0.78,
  },
  dataDescription: {
    marginLeft: 14,
    flex: 1,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 100,
    backgroundColor: 'white'
  },
  dataName: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  dataEmail: {
    fontSize: 12,
    color: '#AEAEAE',
    marginTop: 2,
  },
  dataUsername: {
    fontSize: 14,
    marginTop: 8,
    letterSpacing: 0.7,
  },
  delete: {
    width: 15,
    height: 18,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
})

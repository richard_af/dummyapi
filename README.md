**Step by step :** 

- Clone/Download code nya.
- Setelah itu `npm install` di file Repository agar dapat file node_modules.
- Untuk jaga-jaga antara terinstall atau nggak 3rd party Axios nya, install lagi `npm install axios`
- Coba cek di  ==>  package.json  ==>  "dependencies" apakah Axios nya udah terinstall atau belum.
- Buka App.js lalu run di terminal dengan `npx react-native run-android`, pastikan sudah terhubung dengan HP (usb debugging) atau Emulator.

**Axios :** `$ npm install axios`

**Link Axios :** https://www.npmjs.com/package/react-native-axios

Tutorial CRUD using Axios :

[Create - POST](https://youtu.be/aD6Jhs3CMuc)

[Read - GET](https://youtu.be/LVNxKZxYOD0)

[Update - PUT](https://youtu.be/w6hI0RF9MG4)

[Delete - DELETE](https://youtu.be/IWpIrboBu9g)

**Note :**
- Untuk keterangan lanjutan ada di file (App.js).
- Lengkap untuk penggunaan Fetch JS & Axios.
